// local
import { Fade } from './common';

const Welcome = () => {
    return (
        <Fade>
            <div className="p-4 flex flex-col items-center">
                <h1>Welcome</h1>
                <p>
                    My name is Cameron Wood and I have been a professional 
                    developer for over two years. Recently, I graduated from the University of 
                    Alabama at Birmingham with a B.S. degreee in Computer Science. I have a passion 
                    for learning, programming and making elegrant products.
                </p>
            </div>
        </Fade>  
    );
};

export { Welcome };
