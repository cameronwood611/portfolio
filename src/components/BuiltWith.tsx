
// local 
import { Fade } from './common';

function loadImages() {
    const svgs = require.context ( '../images', true, /\.svg$/ );

    const images = svgs.keys().map((relPath, key) => {
        const title = relPath.split(".")[1].replace("/", "");
        const fullPath =  title + ".svg";
        // format string bc of browserifys static string analysis
        const img = require(`../images/${fullPath}`);
        return (
            <div className="flex flex-col items-center" key={key}>
                <img className="w-20 h-20" src={img.default} alt={title} />
                <h3>{title}</h3>
            </div>
        );
    });
    return (
        <div className="grid grid-cols-3 gap-10 py-8">
            {images}
        </div>
    );
}

function BuiltWith() {
    return (
        <Fade
            delay={500}
            duration={1300}
        >
            <div className="flex flex-col items-center">
                <h2>Built with</h2>
                {loadImages()}
            </div>
        </Fade>
    )
}

export { BuiltWith };