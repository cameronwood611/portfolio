// NPM
import { Navbar, Nav } from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap";


const NavBar = () => {
    return (
      <Navbar expand="lg" className="bg-custom" variant="light">
          <Nav className="mr-auto">
            <LinkContainer to="/">
              <Nav.Link className="link-custom font-bold" eventKey="/">Home</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/projects">
              <Nav.Link className="link-custom" eventKey="/projects">Projects</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/contact">
              <Nav.Link className="link-custom" eventKey="/contact">Contact</Nav.Link>
            </LinkContainer>
          </Nav>
      </Navbar>
    );
};

export { NavBar };
