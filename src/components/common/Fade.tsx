// npm
import styled, { keyframes } from 'styled-components';

const fadeIn = keyframes`
    from { opacity: 0; }
    to { opacity: 1; } `;


const Wrapper = styled.div`
@media (prefers-reduced-motion: no-preference) {
    animation-name: ${fadeIn};
    animation-fill-mode: backwards;
}
`;

type FadeProps = {
    duration?: number;
    delay?: number;
    children: any;
};

const Fade = ({
    duration = 900,
    delay = 200,
    children,
}: FadeProps) => {
    return (
        <Wrapper
            style={{
                animationDuration: duration + 'ms',
                animationDelay: delay + 'ms',

            }}
        >
                {children}
        </Wrapper>
    );
};

export { Fade };