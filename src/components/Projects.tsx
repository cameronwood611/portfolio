// npm
import { Card, Accordion } from 'react-bootstrap';

// local
import { Fade } from './common';


const projects = [
    {
        name: "Profile Analysis",
        desc: "Web tool that aggregated all data gathered from specific profiles on Twitter, Facebook, "
        + "and Instagram for easier analysis of individual platform-specific users. \n\n"
        + "Provided data-driven analytics that included word clouds, hashtag clouds, all posts/tweets, "
        + "data trends regarding how frequently the user posts, and how much other users care about the "
        + "user’s content (via favorites/shares observed on their posts)."
    },
    {
        name: "Complex Fibonacci",
        desc: "A memoized web application devised to demonstrate my knowledge of deploying dockerized containers on AWS."
    },
    {
        name: "Visual Media",
        desc: "Web tool that was devised for observing and analyzing image content scraped.\n"
        + "All the images were assigned a hash (“Perceptual Hash”, as it is known) which "
        + "conveys how the image appears. By relating these hashes, I was able to identify, "
        + "and display, images that were visually similar with little computational effort (KNN algorithm)."
    },
    {
        name: "My résumé",
        desc: "https://gitlab.com/cameronwood611/resume/-/raw/master/resume.pdf"
    },
    {
        name: "This Website",
        desc: "Not sure if this description is necessary but, this is the website you are currently visiting. Thanks for stopping by!"
    },


];

const displayProjects = () => {
    var fadeInc = 0;
    return projects.map((item, key) => {
        fadeInc += 250;
        return (
            <Fade delay={fadeInc}>
                <Card key={key} className="bg-indigo-400 cursor-pointer border-black">
                    <Accordion.Toggle
                        as={Card.Header}
                        variant="link"
                        size="sm"
                        eventKey={String(key)}
                        className="pl-10 text-black"
                    >
                        {item.name}
                    </Accordion.Toggle>
                    <Accordion.Collapse
                        className="bg-indigo-300 bg-opacity-50 cursor-text"
                        eventKey={String(key)}
                    >
                        <Card.Body className="text-lg">{item.desc}</Card.Body>
                    </Accordion.Collapse>
                </Card>
            </Fade>
        );
    });
}


const Projects = () => {
    return (
        <div>
            <h1 className="text-center my-5">Projects</h1>
            <Accordion
                className="w-3/5 my-10 mx-auto"
                defaultActiveKey="">
                {displayProjects()}
            </Accordion>
        </div>
    );
};

export { Projects };
