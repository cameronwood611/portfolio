// npm
import { Button } from 'react-bootstrap';

// local
import { Fade } from './common';


const Contact = () => {
    return (
        <Fade
            delay={0}
        >
            <div className="w-3/5 my-10 mx-auto py-3 text-center flex flex-col items-center bg-indigo-400 rounded-xl">
            <p className="p-4">
                If you are interested in talking with me, please contact me via email 
                by clicking the buttom below!
            </p>
            <a href="mailto:cameron.wood611@gmail.com">
                <Button variant="dark">
                    Email
                </Button>
            </a>
        </div>
        </Fade>
    );
}

export { Contact };