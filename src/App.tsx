
// npm
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


// local
import './App.css';
import {
  Welcome,
  NavBar,
  Projects,
  BuiltWith,
  Contact
} from './components';


function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <NavBar />
      <Switch>
          <Route path="/projects">
            <Projects />
          </Route>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/">
            <Welcome />
            <BuiltWith />
          </Route>
      </Switch>
    </Router>
  );
}

export default App;
